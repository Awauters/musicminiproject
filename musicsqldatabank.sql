CREATE DATABASE `music` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
CREATE TABLE `tracklist` (
  `Track_id` int(11) NOT NULL AUTO_INCREMENT,
  `track_name` varchar(50) NOT NULL,
  `artist` varchar(50) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `album` varchar(50) NOT NULL,
  `studio` varchar(50) NOT NULL,
  `year_edition` int(11) NOT NULL,
  `parental_advisory` varchar(20) NOT NULL,
  `genre_id` int(11) DEFAULT NULL,
  `play_duration` varchar(20) DEFAULT NULL,
  `album_price` decimal(5,2) NOT NULL,
  `image` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`Track_id`),
  KEY `genre_id` (`genre_id`),
  CONSTRAINT `tracklist_ibfk_1` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
