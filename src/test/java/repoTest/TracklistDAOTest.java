package repoTest;

import domain.Tracklist;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repo.TracklistDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class TracklistDAOTest {
    private static final String URL = "jdbc:mysql://localhost:3306/music";
        private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    private Connection connection;
    private TracklistDAO tracklistDAO;
    private Tracklist tracklist;


    @BeforeEach
    public void connectionTest() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            tracklistDAO = new TracklistDAO(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFindAllMovies() {
        List<Tracklist> result = tracklistDAO.findAllTracks();
        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    void showAllTracks() {
        TracklistDAO tracklistDAO = new TracklistDAO(connection);
        List<Tracklist> expected = tracklistDAO.findAllTracks();
        Assertions.assertFalse(expected.isEmpty());
    }
}
