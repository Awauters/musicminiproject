package repoTest;

import domain.Tracklist;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repo.GenreDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class GenreDAOTest {
    private static final String URL = "jdbc:mysql://localhost:3306/music";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    private Connection connection;
    private GenreDAO genreDAO;
    private Tracklist tracklist;


    @BeforeEach
    public void connectionTest() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            genreDAO = new GenreDAO(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
