package services;

import domain.Tracklist;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.TracklistDAO;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class TracklistServiceMockTest {
    @InjectMocks
    private Tracklist tracklistService
            ;

    @Mock
    TracklistDAO tracklistDAO;
    private Tracklist tracklist;

    @BeforeEach
    void setUp() {
        tracklist = new Tracklist();
        Tracklist.setArtist("Vin Diesel");
    }

    @Test
    void findTrackById(int i) throws NoRecordFoundException, IDOutOfBoundException {
        Tracklist Tracklist = null; when(tracklistDAO.findTrackById(anyInt())).thenReturn(Tracklist);
        Tracklist = tracklistService;
        findTrackById(1);
        assertEquals("Vin Diesel", Tracklist.getArtist());
        verify(tracklistDAO, times(1)).findTrackById(1);
        verifyNoMoreInteractions(tracklistDAO);
    }

    @Test
    void noRecordFound() throws NoRecordFoundException, IDOutOfBoundException {
        when(tracklistDAO.findTrackById(anyInt())).thenReturn(null);
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            findTrackById(1);
        });
    }

    @Test
    void idIsLessThan0() {
        Assertions.assertThrows(IDOutOfBoundException.class, () -> {
            findTrackById(0);
        });
    }
}
