package services;

import domain.Genre;
import domain.Tracklist;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.GenreDAO;
import repo.TracklistDAO;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class GenreServiceMockTest {
    @InjectMocks
    private Genre genreService
            ;

    @Mock
    GenreDAO genreDAO;
    private Genre genre;

    @BeforeEach
    void setUp() {
        genre = new Genre();
        Genre.setGenre("Vin Diesel");
    }

    @Test
    void findGenreById(int i) throws NoRecordFoundException, IDOutOfBoundException {
        Genre Genre = null; when(genreDAO.findGenreById(anyInt())).thenReturn(Genre);
        Genre = genreService;
        findTGenreById(1);
        assertEquals("Vin Diesel", Genre.getGenre());
        verify(genreDAO, times(1)).findGenreById(1);
        verifyNoMoreInteractions(genreDAO);
    }

    private void findTGenreById(int i) {
    }

    @Test
    void noRecordFound() throws NoRecordFoundException, IDOutOfBoundException {
        when(genreDAO.findGenreById(anyInt())).thenReturn(null);
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            findGenreById(1);
        });
    }

    @Test
    void idIsLessThan0() {
        Assertions.assertThrows(IDOutOfBoundException.class, () -> {
            findGenreById(0);
        });
    }
}
