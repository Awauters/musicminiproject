package utilities;

import java.sql.*;

public class DatabaseUtil {
    private static final String URL = "jdbc:mysql://localhost:3306/music";
    private static final String USERNAME = "root";

    public static Connection createConnectionIfClosed(Connection connection) {
        try {
            if (connection == null || connection.isClosed()) {
                return DriverManager.getConnection(URL, USERNAME, "");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void closeConnections(Connection con, ResultSet set, PreparedStatement statement) {
        try {
            if (set != null) {
                set.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (con != null) {
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

