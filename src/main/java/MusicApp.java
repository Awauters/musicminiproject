import application.Music;
import application.Music;
import repo.GenreDAO;
import repo.TracklistDAO;
import services.GenreService;
import services.TracklistService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MusicApp {
    public static void main(String[] args) {
       String URL = "jdbc:mysql://localhost:3306/games";
         String USERNAME = "root";
        Connection connection = null;
        try {
             connection = DriverManager.getConnection(URL, USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        GenreDAO gDAO = new GenreDAO();
        GenreService genreService = new GenreService(gDAO);

        TracklistDAO tDAO = new TracklistDAO();
        TracklistService tracklistService = new TracklistService(tDAO);

        /**
         * starts musicApp
         */
        Music music = new Music(connection, genreService, tracklistService);
        music.startMusic();

    }
}
