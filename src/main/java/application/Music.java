package application;

import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.GenreDAO;
import repo.TracklistDAO;
import services.GenreService;
import services.TracklistService;

import java.sql.Connection;
import java.util.*;

public class Music {
    private Connection connection;
    private Map<Integer, String> menuMap;
    private Scanner scanner;
    private TracklistService tracklistService;
    private String match;
    private int TrackId;
    private String input;
    private GenreService genreService;

    public Music(Connection connection, GenreService genreService, TracklistService tracklistService) {
        this.connection = connection;
        this.scanner = new Scanner(System.in);
        this.menuMap = new HashMap<>();
        this.genreService = genreService;
        this.tracklistService = tracklistService;
//      menuMap.put(1, "Find and show tracks by genre"); nullpointerexception couldn't find it so skipped it
        menuMap.put(2, "show the 4th track.");
//      menuMap.put(3, "find a track of your choice."); nullpointerexception couldn't find it so skipped it
//      menuMap.put(4, "find a track by it's id."); shows id 0 doesn't give input so skipped it
        menuMap.put(5, "find a track by it's album.");
        menuMap.put(6, "show all tracks.");
    }


    public void startMusic() {
        System.out.println("-------------HELLO CUSTOMER--------------");
        System.out.println("Tracklist options");
        showMenu();
    }

    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        chooseOption();
    }

    private void chooseOption() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        try {
            System.out.print("Choose your option : ");
            int i = scanner.nextInt();
            executeOption(i);
        } catch (InputMismatchException e) {
            System.err.println("I asked for a f*ing number!!!");
            scanner = null;
            chooseOption();
        }
    }

    protected void executeOption(int option) {
        boolean continueGame = true;
        switch (option) {
//            case 1:
//                findGenreStartingWith(input);
//                break;
            case 2:
                findSpecificTrack(4);
                break;
//            case 3:
//                findTrackStartingWith(match);
//                break;
//            case 4:
//                findTrackById(TrackId);
//                break;

            case 5:
                insertAndFindAlbumByName();
                break;

            case 6:
                showAllTracks();
                break;
        }
        if (continueGame) {
            showMenu();
        }
    }

//    private void findTrackStartingWith(String match) {
//        try {
//            domain.Tracklist tracklist = tracklistService.findTrackStartingWith(match);
//            System.out.println("----- TRACK INFORMATION -----");
//            System.out.println("TRACK NAME:\t\t" + tracklist.getTrackName());
//            System.out.println("ARTIST:\t" + tracklist.getArtist());
//            System.out.println("ALBUM:\t\t" + tracklist.getAlbum());
//            System.out.println("ALBUM PRICE:\t\t" + tracklist.getAlbumPrice());
//            System.out.println("-----------------------------");
//        } catch (NoRecordFoundException e) {
//            System.out.println(" /!\\ No matching tracks were found, sorry. ");
//        }
//    }

//    private void findTrackById(int TrackId) {
//        try {
//            System.out.println("Showing Track with ID " + TrackId);
//            domain.Tracklist tracklist = tracklistService.findTrackById(TrackId);
//            System.out.println("----- TRACK INFORMATION -----");
//            System.out.println("TRACK NAME:\t\t" + tracklist.getTrackName());
//            System.out.println("ARTIST:\t" + tracklist.getArtist());
//            System.out.println("ALBUM:\t\t" + tracklist.getAlbum());
//            System.out.println("ALBUM PRICE:\t\t" + tracklist.getAlbumPrice());
//            System.out.println("-----------------------------");
//        } catch (IDOutOfBoundException e) {
//            System.out.println(" /!\\ Sorry, that ID is either too small or too big ");
//        } catch (NoRecordFoundException e) {
//            System.out.println(" /!\\ We couldn't find any tracks with this ID, sorry. ");
//        }
//    }

    private void showAllTracks() {
        try {
            System.out.println("Loading, Please wait...");
            List<domain.Tracklist> tracks = tracklistService.findAllTracks();
            System.out.println("----- ALL TRACKS -----");
            for (domain.Tracklist tracklist : tracks) {
                System.out.println("-----------------------------");
                System.out.println("ID:\t\t" + tracklist.getTrackName());
                System.out.println("ARTIST:\t" + tracklist.getArtist());
                System.out.println("ALBUM PRICE:\t\t" + tracklist.getAlbumPrice());
                System.out.println("-----------------------------");
            }
        } catch (NoRecordFoundException e) {
            System.out.println(" /!\\ No tracks were found, sorry. ");
        }
    }

//    private void findGenreStartingWith(String match) {
//        try {
//            domain.Genre genre = genreService.findGenreStartingWith(match);
//            System.out.println("----- GENRE INFORMATION -----");
//            System.out.println("GENRE ID:\t\t" + genre.getGenreId());
//            System.out.println("GENRE NAME:\t" + genre.getGenre());
//            System.out.println("-----------------------------");
//        } catch (NoRecordFoundException e) {
//            System.out.println(" /!\\ No matching tracks were found, sorry. ");
//        }
//    }

    /**
     * finds a specified track based on it's id (4)
     * @param i is the id 4
     * @return the track with the specified id (4)
     */
    private void findSpecificTrack(int i) {
        try {
            System.out.println("Showing track with ID " + i);
            domain.Tracklist tracklist = tracklistService.findTrackById(i);
            System.out.println("----- MUSIC INFORMATION -----");
            System.out.println("TRACK NAME:\t\t" + tracklist.getTrackName());
            System.out.println("ARTIST:\t" + tracklist.getArtist());
            System.out.println("ALBUM:\t\t" + tracklist.getAlbum());
            System.out.println("ALBUM PRICE:\t\t" + tracklist.getAlbumPrice());
            System.out.println("-----------------------------");
        } catch (IDOutOfBoundException e) {
            System.out.println(" /!\\ Sorry, that ID is either too small (or too big) ");
        } catch (NoRecordFoundException e) {
            System.out.println(" /!\\ We couldn't find any tracks with this ID, sorry. ");
        }
    }

    /**
     * insert an input and searches for any matching album, listing any tracks of said album
     * @return the specified album
     */
    private void insertAndFindAlbumByName() {
        System.out.print("\nSearch: ");
        String match = scanner.next();
        System.out.println("Searching...");
        findAlbumStartingWith(match);
    }

    private void findAlbumStartingWith(String match) {
        try {
            domain.Tracklist tracklist = tracklistService.findAlbumStartingWith(match);
            System.out.println("----- ALBUM INFORMATION -----");
            System.out.println("ALBUM:\t\t" + tracklist.getAlbum());
            System.out.println("ARTIST:\t" + tracklist.getArtist());
            System.out.println("TRACK NAME:\t\t" + tracklist.getTrackName());
            System.out.println("ALBUM PRICE:\t\t" + tracklist.getAlbumPrice());
            System.out.println("-----------------------------");
        } catch (NoRecordFoundException e) {
            System.out.println(" /!\\ No matching albums were found, sorry. ");
        }
    }
}