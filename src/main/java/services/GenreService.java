package services;

import domain.Genre;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.GenreDAO;

public class GenreService {
    private GenreDAO genreDAO;

    public GenreService(GenreDAO genreDAO){
        this.genreDAO = genreDAO;
    }


    /**
     *finds a genre by it's id
     * @param id the id of the genre
     * @return wanted output
     * @throws IDOutOfBoundException
     * @throws NoRecordFoundException
     */
    public Genre findGenreById(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if(id <= 0){
            throw new IDOutOfBoundException();
        } else {
            Genre genre = genreDAO.findGenreById(id);
            if(genre == null){
                throw new NoRecordFoundException();
            } else {
                return genre;
            }
        }
    }

    /**
     * searches for and displays the first genre by the input you inserted
     * @param match
     * @return wanted input
     * @throws NoRecordFoundException
     */
    public Genre findFirstGenreStartingWith(String match) throws NoRecordFoundException {
        if (match.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            Genre genre = genreDAO.findFirstGenreStartingWith(match);
            if (genre == null) {
                throw new NoRecordFoundException();
            } else {
                return genre;
            }
        }
    }

    /**
     *  searches for a genre starting with a given input
     * @param match
     * @return wanted result
     * @throws NoRecordFoundException
     */
    public Genre findGenreStartingWith(String match) throws NoRecordFoundException {
        if (match.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            Genre genre = genreDAO.findFirstGenreStartingWith(match);
            if (genre == null) {
                throw new NoRecordFoundException();
            } else {
                return genre;
            }
        }
    }
}
