package services;

import domain.Tracklist;
import domain.Tracklist;
import domain.Tracklist;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.TracklistDAO;

import java.util.List;


public class TracklistService {
    private TracklistDAO tracklistDAO;

    public TracklistService(TracklistDAO tracklistDAO) {
        this.tracklistDAO = tracklistDAO;
    }


    public Tracklist findFirstTrackStartingWith(String match) throws NoRecordFoundException {
        if (match.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            Tracklist music = tracklistDAO.findFirstTrackStartingWith(match);
            if (music == null) {
                throw new NoRecordFoundException();
            } else {
                return music;
            }
        }
    }

    public Tracklist findTrackStartingWith(String match) throws NoRecordFoundException {
        if (match.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            Tracklist music = tracklistDAO.findFirstTrackStartingWith(match);
            if (music == null) {
                throw new NoRecordFoundException();
            } else {
                return music;
            }
        }
    }

    public Tracklist findTrackById(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if (id < 1) {
            throw new IDOutOfBoundException();
        } else {
            Tracklist music = tracklistDAO.findTrackById(id);
            if (music == null) {
                throw new NoRecordFoundException();
            } else {
                return music;
            }
        }
    }
    public List<Tracklist> findAllTracks() throws NoRecordFoundException {
        List<Tracklist> tracks = tracklistDAO.findAllTracks();

        if(tracks.isEmpty()){
            throw new NoRecordFoundException();
        } else {
            return tracks;
        }
    }

//    public String findTrackById(int track_id) throws IDOutOfBoundException, NoRecordFoundException {
//        if (track_id < 1) {
//            throw new IDOutOfBoundException();
//        } else  {
//            Tracklist music = tracklistDAO.findTrackById(track_id);
//            if (music == null) {
//                throw new NoRecordFoundException();
//            } else {
//                return Tracklist;
//            }
//        }
//    }


    public Tracklist findAlbumStartingWith(String match) throws NoRecordFoundException {
        if(match.isEmpty()){
            throw new IllegalArgumentException();
        } else {
            Tracklist music = tracklistDAO.findAlbumStartingWith(match);
            if(music == null){
                throw new NoRecordFoundException();
            } else {
                return music;
            }
        }
    }
}

