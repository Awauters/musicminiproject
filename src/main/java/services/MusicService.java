//package services;
//
//import domain.Music;
//import exceptions.IDOutOfBoundException;
//import exceptions.NoRecordFoundException;
//import repo.MusicDAO;
//
//import java.util.List;
//
//
//public class MusicService {
//    private MusicDAO musicDAO = new MusicDAO();
//
//    public MusicService(MusicDAO musicDAO) {
//
//    }
//
//    public MusicService() {
//
//    }
//
//    public Music findFirstTrackStartingWith(String match) throws NoRecordFoundException {
//        if (match.isEmpty()) {
//            throw new IllegalArgumentException();
//        } else {
//            Music music = musicDAO.findFirstTrackStartingWith(match);
//            if (music == null) {
//                throw new NoRecordFoundException();
//            } else {
//                return music;
//            }
//        }
//    }
//
//    public Music findTrackStartingWith(String match) throws NoRecordFoundException {
//        if (match.isEmpty()) {
//            throw new IllegalArgumentException();
//        } else {
//            Music music = musicDAO.findFirstTrackStartingWith(match);
//            if (music == null) {
//                throw new NoRecordFoundException();
//            } else {
//                return music;
//            }
//        }
//    }
//
//    public Music findTrackById(int id) throws IDOutOfBoundException, NoRecordFoundException {
//        if (id < 1) {
//            throw new IDOutOfBoundException();
//        } else {
//            Music music = musicDAO.findTrackById(id);
//            if (music == null) {
//                throw new NoRecordFoundException();
//            } else {
//                return music;
//            }
//        }
//    }
//    public List<Music> findAllTracks() throws NoRecordFoundException {
//        List<Music> tracks = musicDAO.findAllTracks();
//
//        if(tracks.isEmpty()){
//            throw new NoRecordFoundException();
//        } else {
//            return tracks;
//        }
//    }
//
////    public String findTrackById(int track_id) throws IDOutOfBoundException, NoRecordFoundException {
////        if (track_id < 1) {
////            throw new IDOutOfBoundException();
////        } else  {
////            Music music = musicDAO.findTrackById(track_id);
////            if (music == null) {
////                throw new NoRecordFoundException();
////            } else {
////                return Music;
////            }
////        }
////    }
//
//
//    public Music findAlbumStartingWith(String match) throws NoRecordFoundException {
//        if(match.isEmpty()){
//            throw new IllegalArgumentException();
//        } else {
//            Music music = musicDAO.findAlbumStartingWith(match);
//            if(music == null){
//                throw new NoRecordFoundException();
//            } else {
//                return music;
//            }
//        }
//    }
//}
//
