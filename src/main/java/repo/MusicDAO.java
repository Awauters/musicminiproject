//package repo;
//
//import domain.Music;
//import domain.Tracklist;
//import exceptions.IDOutOfBoundException;
//import exceptions.NoRecordFoundException;
//import services.GenreService;
//import services.MusicService;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//import static utilities.DatabaseUtil.closeConnections;
//import static utilities.DatabaseUtil.createConnectionIfClosed;
//
//public class MusicDAO {
//    Connection connection;
//    private PreparedStatement preparedStatement;
//    private ResultSet resultSet;
//    private MusicDAO musicDAO;
//
//    public Music findFirstTrackStartingWith(String match) {
//        Music music = new Music();
//        try {
//            String query = "SELECT * FROM Music m " +
//                    "LEFT JOIN genre g ON g.genre_id = g.id " +
//                    "WHERE track_name LIKE ? " +
//                    "ORDER BY track_name ASC LIMIT 1";
//            connection = createConnectionIfClosed(connection);
//            preparedStatement = connection.prepareStatement(query);
//            preparedStatement.setString(1, match + "%");
//            preparedStatement.execute();
//
//            resultSet = preparedStatement.getResultSet();
//            if (resultSet.next()) {
//                music = this.createObjectFromSQL(music, resultSet, musicDAO);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (NoRecordFoundException e) {
//            e.printStackTrace();
//        } catch (IDOutOfBoundException e) {
//            e.printStackTrace();
//        } finally {
//            closeConnections(connection, resultSet, preparedStatement);
//
//        }
//        return music;
//    }
//
//    public Music findTrackById(int id) {
//        Music music = new Music();
//        try {
//            String query = "SELECT * FROM music m " +
//                    "LEFT JOIN genre g ON g.genre_id = g.id " +
//                    "WHERE g.id = ? ";
//            connection = createConnectionIfClosed(connection);
//            preparedStatement = connection.prepareStatement(query);
//            preparedStatement.setInt(1, id);
//            preparedStatement.execute();
//            resultSet = preparedStatement.getResultSet();
//
//            if (resultSet.next()) {
//                music = this.createObjectFromSQL(music, resultSet);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            closeConnections(connection, resultSet, preparedStatement);
//        }
//        return music;
//    }
//
//    private Tracklist createObjectFromSQL(Tracklist tracklist, ResultSet resultSet) {
//        return tracklist;
//    }
//
//    private Tracklist createObjectFromSQL(Tracklist music, ResultSet resultSet, MusicDAO musicDAO) throws IDOutOfBoundException, NoRecordFoundException {
//        try {
//            music = new Music();
//            music.setTrackId(resultSet.getInt("track_id"));
//            music.setTrackName(resultSet.getString("track_name"));
//            music.setArtist(resultSet.getString("artist"));
//            music.setGenre(resultSet.getString("genre"));
//            music.setStudio(resultSet.getString("studio"));
//            music.setAlbum(resultSet.getString("album"));
//            music.setYearEdition(resultSet.getInt("year_edition"));
//            music.setParentalAdvisory(resultSet.getString("parental_advisory"));
//            music.setTrackDuration(resultSet.getString("track_duration"));
//            music.setAlbumPrice(resultSet.getFloat("price"));
//            music.setImage(resultSet.getString("image"));
//
//            if (resultSet.getInt("track_id") == 0) {
//                music.setMusic(null);
//            } else {
//                musicDAO = new MusicDAO();
//                MusicService MusicService = new MusicService(musicDAO);
//                Music m = MusicService.findTrackById(resultSet.getInt("track_id"));
//                music.setGenre(m);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return music;
//    }
//    public List<Music> findAllTracks() {
//        List<Music> tracks = new ArrayList<>();
//        try {
//            String query = "SELECT * FROM music m " +
//                    "LEFT JOIN genre g ON g.genre_id = g.id " +
//                    "ORDER BY game_name ASC ";
//            connection = createConnectionIfClosed(connection);
//            preparedStatement = connection.prepareStatement(query);
//            preparedStatement.execute();
//            resultSet = preparedStatement.getResultSet();
//            while (resultSet.next()) {
//                Music music = new Music();
//                music = this.createObjectFromSQL(music, resultSet);
//
//                music.add(music);
//            }
//
//            return tracks;
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            closeConnections(connection, resultSet, preparedStatement);
//
//        }
//        return tracks;
//    }
//    public Music findAlbumStartingWith(String match) {
//        Music music = new Music();
//        try {
//            String query = "SELECT * FROM music m " +
//                    "LEFT JOIN genre g ON g.genre_id = g.id " +
//                    "WHERE album LIKE ? " +
//                    "ORDER BY album ASC LIMIT 1";
//            connection = createConnectionIfClosed(connection);
//            preparedStatement = connection.prepareStatement(query);
//            preparedStatement.setString(1, match + "%");
//            preparedStatement.execute();
//
//            resultSet = preparedStatement.getResultSet();
//            if (resultSet.next()) {
//                music = this.createObjectFromSQL(music, resultSet);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            closeConnections(connection, resultSet, preparedStatement);
//
//        }
//        return music;
//    }
//}
//
