package repo;

import domain.Genre;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import services.GenreService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static utilities.DatabaseUtil.closeConnections;
import static utilities.DatabaseUtil.createConnectionIfClosed;

public class GenreDAO {
    private Connection connection;
    private PreparedStatement statement;
    private PreparedStatement s;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public GenreDAO(Connection connection) {
    }

    /**
     *  searches for a genre by it's inherent id
     * @param id
     * @return the wanted result found by searching
     */
    public Genre findGenreById(int id) {
        Genre genre = new Genre();
        try {
            connection = createConnectionIfClosed(connection);
            statement = connection.prepareStatement("SELECT * FROM genre WHERE id = ?");
            statement.setInt(1, id);
            statement.execute();
            resultSet = statement.getResultSet();
            if (resultSet.next()) {
                genre = new Genre();
                genre.setGenreId(resultSet.getInt("id"));
                genre.setGenre(resultSet.getString("genre_name"));
            }
            return genre;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(connection, resultSet, statement);
        }
        return genre;
    }

    /**
     * finds the first genre starting with own input, either a letter or a string
     * @param match
     * @return the result that was found by searching
     */
    public Genre findFirstGenreStartingWith(String match) {
        Genre genre = null;
        try {
            String query = "SELECT * FROM Genre g " +
                    "WHERE genre_name LIKE ? " +
                    "ORDER BY genre_name ASC LIMIT 1";
            connection = createConnectionIfClosed(connection);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, match + "%");
            preparedStatement.execute();

            resultSet = preparedStatement.getResultSet();
            if (resultSet.next()) {
                genre = this.createObjectFromSQL(genre, resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(connection, resultSet, preparedStatement);

        }
        return genre;
    }

    /**
     * creates genre object
     * @param genre
     * @param resultSet creates location
     * @return expected outcome
     */
    private Genre createObjectFromSQL(Genre genre, ResultSet resultSet) {
        try {
            genre = new Genre();
            genre.setGenreId(resultSet.getInt("genre_id"));
            genre.setGenre(resultSet.getString("genre_name"));


            if (resultSet.getString("genre") == null) {
                genre.setGenre((Genre) null);
            } else {
                GenreService genreService = new GenreService(null);
                Genre g = genreService.findGenreStartingWith(resultSet.getString("genre"));
                genre.setGenre(g);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        } finally {
            closeConnections(connection, resultSet, preparedStatement);
        }
        return genre;
    }
}