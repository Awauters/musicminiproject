package repo;

import domain.Tracklist;
import domain.Tracklist;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import services.GenreService;
import services.TracklistService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static utilities.DatabaseUtil.closeConnections;
import static utilities.DatabaseUtil.createConnectionIfClosed;

public class TracklistDAO {
    Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private TracklistDAO tracklistDAO;

    public TracklistDAO(Connection connection) {
    }

    public Tracklist findFirstTrackStartingWith(String match) {
        Tracklist tracklist = new Tracklist();
        try {
            String query = "SELECT * FROM tracklist t " +
                    "LEFT JOIN genre g ON genre_id = g.id " +
                    "WHERE track_name LIKE ? " +
                    "ORDER BY track_name ASC LIMIT 1";
            connection = createConnectionIfClosed(connection);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, match + "%");
            preparedStatement.execute();

            resultSet = preparedStatement.getResultSet();

            if (resultSet.next()) {
                tracklist = this.createObjectFromSQL(tracklist, resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        } catch (IDOutOfBoundException e) {
            e.printStackTrace();
        } finally {
            closeConnections(connection, resultSet, preparedStatement);

        }
        return tracklist;
    }

    public Tracklist findTrackById(int id) {
        Tracklist tracklist = new Tracklist();
        try {
            String query = "SELECT * FROM tracklist m " +
                    "LEFT JOIN genre g ON genre_id = g.id " +
                    "WHERE g.id = ? ";
            connection = createConnectionIfClosed(connection);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            resultSet = preparedStatement.getResultSet();

            if (resultSet.next()) {
                tracklist = this.createObjectFromSQL(tracklist, resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        } catch (IDOutOfBoundException e) {
            e.printStackTrace();
        } finally {
            closeConnections(connection, resultSet, preparedStatement);
        }
        return tracklist;
    }


    private Tracklist createObjectFromSQL(Tracklist tracklist, ResultSet resultSet) throws IDOutOfBoundException, NoRecordFoundException {
        try {
            tracklist = new Tracklist();
            tracklist.setTrackId(resultSet.getInt("track_id"));
            tracklist.setTrackName(resultSet.getString("track_name"));
            tracklist.setArtist(resultSet.getString("artist"));
            tracklist.setGenre(resultSet.getString("genre"));
            tracklist.setStudio(resultSet.getString("studio"));
            tracklist.setAlbum(resultSet.getString("album"));
            tracklist.setYearEdition(resultSet.getInt("year_edition"));
            tracklist.setParentalAdvisory(resultSet.getString("parental_advisory"));
            tracklist.setTrackDuration(resultSet.getString("play_duration"));
            tracklist.setAlbumPrice(resultSet.getFloat("album_price"));
            tracklist.setImage(resultSet.getString("image"));
            tracklist.setGenreId(resultSet.getInt("genre_id"));
//            if (resultSet.getInt("track_id") == 0) {
//                tracklist.setMusic(null);
//            } else {
//                tracklistDAO = new TracklistDAO();
//                TracklistService tracklistService = new TracklistService(tracklistDAO);
//                Tracklist t = services.TracklistService.findTrackById(resultSet.getInt("track_id"));
//                tracklist.setGenre(t);
//            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tracklist;
    }
    public List<Tracklist> findAllTracks() {
        List<Tracklist> tracks = new ArrayList<>();
        try {
            String query = "SELECT * FROM tracklist t " +
                    "LEFT JOIN genre g ON genre_id = g.id " +
                    "ORDER BY track_name ASC ";
            connection = createConnectionIfClosed(connection);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.execute();
            resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                Tracklist tracklist = new Tracklist();
                tracklist = this.createObjectFromSQL(tracklist, resultSet);

                tracks.add(tracklist);
            }

            return tracks;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        } catch (IDOutOfBoundException e) {
            e.printStackTrace();
        } finally {
            closeConnections(connection, resultSet, preparedStatement);

        }
        return tracks;
    }
    public Tracklist findAlbumStartingWith(String match) {
        Tracklist tracklist = new Tracklist();
        try {
            String query = "SELECT * FROM tracklist t " +
                    "LEFT JOIN genre g ON genre_id = g.id " +
                    "WHERE album LIKE ? " +
                    "ORDER BY album ASC LIMIT 1";
            connection = createConnectionIfClosed(connection);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, match + "%");
            preparedStatement.execute();

            resultSet = preparedStatement.getResultSet();
            if (resultSet.next()) {
                tracklist = this.createObjectFromSQL(tracklist, resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        } catch (IDOutOfBoundException e) {
            e.printStackTrace();
        } finally {
            closeConnections(connection, resultSet, preparedStatement);

        }
        return tracklist;
    }
}

