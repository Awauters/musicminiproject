
package domain;

public class Genre {
    private int GenreId;
    private String genre;

    public Genre(int id, String genre) {
        this.GenreId = id;
        this.genre = genre;
    }

    public Genre() {
    }

    public int getGenreId() {
        return GenreId;
    }

    public void setGenreId(int genreId) {
        GenreId = genreId;
    }

    public String getGenre() {
        return genre;
    }

    public static void setGenre(String genre) {
        this.genre = genre;
    }

    public void setGenre(Genre g) {
    }
}

