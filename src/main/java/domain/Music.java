//package domain;
//
//public class Music {
//
//
//    public int TrackId;
//    public String TrackName;
//    private String artist;
//    private String genre;
//    private String studio;
//    private String album;
//    private int YearEdition;
//    private String parentalAdvisory;
//    private int genreId;
//    private String trackDuration;
//    private float albumPrice;
//    private String image;
//
//    public Music(int TrackId,
//                 String TrackName,
//                 String artist,
//                 String genre,
//                 String studio,
//                 String album,
//                 int YearEdition,
//                 String parentalAdvisory,
//                 int genreId,
//                 String trackDuration,
//                 float albumPrice,
//                 String image) {
//        this.TrackId = TrackId;
//        this.TrackName = TrackName;
//        this.artist = artist;
//        this.genre = genre;
//        this.studio = studio;
//        this.album = album;
//        this.YearEdition = YearEdition;
//        this.parentalAdvisory = parentalAdvisory;
//        this.genreId = genreId;
//        this.trackDuration = trackDuration;
//        this.albumPrice = albumPrice;
//        this.image = image;
//    }
//
//    public Music() {
//
//    }
//
//    @Override
//    public String toString() {
//        return "Game{" +
//                "TrackId=" + TrackId +
//                ", TrackName='" + TrackName + '\'' +
//                ", artist='" + artist + '\'' +
//                ", genre='" + genre + '\'' +
//                ", studio=" + studio +
//                ", album='" + album + '\'' +
//                ", YearEdition=" + YearEdition +
//                ", parentalAdvisory=" + parentalAdvisory +
//                ", genreId=" + genreId +
//                ", trackDuration='" + trackDuration + '\'' +
//                ", albumPrice=" + albumPrice +
//                ", image='" + image + '\'' +
//                '}';
//    }
//
//    public int getTrackId() {
//        return TrackId;
//    }
//
//    public void setTrackId(int trackId) {
//        TrackId = trackId;
//    }
//
//    public String getTrackName() {
//        return TrackName;
//    }
//
//    public void setTrackName(String TrackName) {
//        this.TrackName = TrackName;
//    }
//
//    public String getArtist() {
//        return artist;
//    }
//
//    public void setArtist(String artist) {
//        this.artist = artist;
//    }
//
//    public String getGenre() {
//        return genre;
//    }
//
//    public void setGenre(String genre) {
//        this.genre = genre;
//    }
//
//    public String getStudio() {
//        return studio;
//    }
//
//    public void setStudio(String studio) {
//        this.studio = studio;
//    }
//
//    public String getAlbum() {
//        return album;
//    }
//
//    public void setAlbum(String album) {
//        this.album = album;
//    }
//
//    public int getYearEdition() {
//        return YearEdition;
//    }
//
//    public void setYearEdition(int yearEdition) {
//        YearEdition = yearEdition;
//    }
//
//    public String getParentalAdvisory() {
//        return parentalAdvisory;
//    }
//
//    public void setParentalAdvisory(String parentalAdvisory) {
//        this.parentalAdvisory = parentalAdvisory;
//    }
//
//    public int getGenreId() {
//        return genreId;
//    }
//
//    public void setGenreId(int genreId) {
//        this.genreId = genreId;
//    }
//
//    public String getTrackDuration() {
//        return trackDuration;
//    }
//
//    public void setTrackDuration(String trackDuration) {
//        this.trackDuration = trackDuration;
//    }
//
//    public float getAlbumPrice() {
//        return albumPrice;
//    }
//
//    public void setAlbumPrice(float albumPrice) {
//        this.albumPrice = albumPrice;
//    }
//
//    public String getImage() {
//        return image;
//    }
//
//    public void setImage(String image) {
//        this.image = image;
//    }
//
//    public void add(Music music) {
//    }
//    public void setMusic(Object o) {
//    }
//
//    public void setGenre(Music m) {
//    }
//}
